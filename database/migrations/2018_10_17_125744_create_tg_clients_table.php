<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTgClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tg_clients', function (Blueprint $table) {
            $table->integer('id');
            $table->primary('id');
            $table->string('first');
            $table->string('last');
            $table->string('username')->nullable($value = true);
            $table->string('degree')->nullable($value = true);
            $table->integer('course')->nullable($value = true);
            $table->integer('spec_id')->nullable($value = true);
            $table->integer('block_id')->nullable($value = true);
            $table->string('state')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tg_clients');
    }
}
