<?php

namespace App;

class TgButton
{
    public static function create($text, $data)
    {
        return [
            'text' => $text,
            'callback_data' => $data,
        ];
    }
}
