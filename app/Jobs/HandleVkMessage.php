<?php

namespace App\Jobs;

use App\Button;
use App\Client;
use App\Data;
use App\Keyboard;
use App\Parser;
use App\Timetable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class HandleVkMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function handle()
    {
        // Если клиента нет в системе, регистрируем клиента
        $client = Client::find($this->object->user_id);

        if ($client === null) {
            $client = new Client();
            $client->signUp($this->object->user_id);
        }

        $command = '';
        $payload = [];

        if (property_exists($this->object, 'payload')) {
            $payload = json_decode($this->object->payload);
            $command = $payload->command;
        }

        switch ($command) {
            case 'start':
                $degreeBtns = [];

                foreach (array_keys(Data::DEGREE_COURSES) as $degree) {
                    $degreeBtns[] = Button::create(
                            json_encode(
                                [
                                    'command'  => 'degree',
                                    'degree'   => $degree,
                                ]
                            ),
                            $degree,
                            'primary'
                    );
                }

                $client->sendKeyboard(
                    Keyboard::create($degreeBtns),
                    'Че там?'
                );

                break;
            case 'degree':
                $client->degree = $payload->degree;
                $courses        = Data::DEGREE_COURSES[$payload->degree];
                $courseBtns     = [];

                if ($client->save()) {
                    foreach ($courses as $course) {
                        $courseBtns[] = Button::create(
                            json_encode(
                                [
                                    'command'  => 'course',
                                    'course'   => $course,
                                ]
                            ),
                            $course,
                            'primary'
                        );
                    }

                    $client->sendKeyboard(
                        Keyboard::create($courseBtns),
                        'Курс'
                    );
                } else {
                    $client->send('Произошла ошибка');
                }

                break;
            case 'course':
                $client->course = $payload->course;
                $allSpecs       = Parser::getSpecs($payload->course);
                $specBtns       = [];

                if ($client->save()) {
                    foreach ($allSpecs as $spec) {
                        $specName = explode(' ', $spec->name_en)[0];
                        $suffix   = $specName[0];

                        if (($suffix == 'm' && $client->degree == 'master') ||
                            ($suffix == 'd' && $client->degree == 'doctor') ||
                            ($suffix != 'm' && $suffix != 'd' && $client->degree == 'bachelor'))
                        {
                            $specBtns[] = Button::create(
                                json_encode(
                                    [
                                        'command'  => 'spec',
                                        'spec_id'  => $spec->id,
                                    ]
                                ),
                                $specName,
                                'primary'
                            );
                        } else {
                            continue;
                        }
                    }

                    $client->sendKeyboard(
                        Keyboard::create($specBtns),
                        'Специальность'
                    );
                }

                break;
            case 'spec':
                $client->spec_id = $payload->spec_id;
                $allBlocks       = Parser::getBlocks($client->course, $payload->spec_id);
                $blockBtns       = [];

                if ($client->save()) {
                    foreach ($allBlocks as $block) {
                        $blockBtns[] = Button::create(
                            json_encode(
                                [
                                    'command'  => 'block',
                                    'block_id' => $block->id,
                                ]
                            ),
                            $block->name_en,
                            'primary'
                        );
                    }

                    $client->sendKeyboard(
                        Keyboard::create($blockBtns),
                        'Специальность'
                    );
                }

                break;
            case 'block':
                $client->block_id = $payload->block_id;

                if ($client->save()) {
                    $client->sendKeyboard(
                        Keyboard::main(),
                        'Расписание на'
                    );
                }

                break;
            case 'today':
                $client->send("Подожди, я смотрю 🤓");

                $index   = date('N', time());
                $message = '';

                if ($index == '6' || $index == '7') {
                    $client->send('Сегодня нет занятий 🎉');
                } else {
                    $data = Parser::getBlockData($client->block_id);

                    foreach ($data['timetable'][$index] as $row) {
                        $message .= Timetable::prepareMessage($data, $row);
                    }

                    $client->send($message);
                }

                break;
            case 'tomorrow':
                $client->send("Подожди, я смотрю 🤓");

                $index   = (date('N', time()) % 7) + 1;
                $message = '';

                if ($index == '6' || $index == '7') {
                    $client->send('Завтра нет занятий 🎉');
                } else {
                    $data = Parser::getBlockData($client->block_id);

                    foreach ($data['timetable'][$index] as $row) {
                        $message .= Timetable::prepareMessage($data, $row);
                    }

                    $client->send($message);
                }

                break;
            case 'week':
                $client->send("Подожди, я смотрю 🤓");

                $data    = Parser::getBlockData($client->block_id);
                $message = '';

                foreach ($data['timetable'] as $key => $day) {
                    $message .= "📅 " . Timetable::WEEK_DAYS[$key] . "\n";

                    foreach ($day as $row) {
                        $message .= Timetable::prepareMessage($data, $row);
                    }

                    $client->send($message);

                    $message = '';
                }

                break;

            case 'teachers':
                $data        = Parser::getBlockData($client->block_id);
                $teacherBtns = [];

                foreach ($data['teachers'] as $key => $teacher) {
                    $teacherBtns[] = Button::create(
                        json_encode(
                            [
                                'command'    => 'teacher',
                                'teacher_id' => $key,
                            ]
                        ),
                        $teacher['teacher_en'],
                        'primary'
                    );
                }

                $client->sendKeyboard(
                    Keyboard::create($teacherBtns),
                    'Расписание для'
                );

                break;

            case 'teacher':
                $client->send("Подожди, я смотрю 🤓");

                $data    = Parser::getTeacherData($payload->teacher_id);
                $message = '';

                foreach ($data['timetable'] as $key => $day) {
                    $message .= "📅 " . Timetable::WEEK_DAYS[$key] . "\n";

                    foreach ($day as $row) {
                        $message .= Timetable::prepareMessage($data, $row);
                    }

                    $client->send($message);

                    $message = '';
                }

                $client->sendKeyboard(
                    Keyboard::main(),
                    'Расписание на'
                );

                break;

            default:
                $client->send("Я не понимаю, нажимай на кнопки ");

                break;
        }
    }
}
