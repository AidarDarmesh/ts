<?php

namespace App\Jobs;

use App\TgKeyboard;
use App\TgButton;
use App\TgClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class HandleTgMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function handle()
    {
        // Если клиента нет в системе, регистрируем клиента
        $tgClient = TgClient::find($this->object->from->id);

        if ($tgClient === null) {
            $tgClient = new TgClient();
            $tgClient->signUp($this->object->from);
        }

        switch ($this->object->text) {
            case "/start" || "start":
                $keyboard = TgKeyboard::create(
                    [
                        TgButton::create('hello', 123),
                        TgButton::create('bye', 456),
                    ]
                );

                $tgClient->send('choose', $keyboard);

                break;
            default:
                $tgClient->send('Не понимаю, нажимай на кнопки');
                break;
        }
    }
}
