<?php

namespace App;

use Illuminate\Support\Facades\Log;

class Parser
{
    const URL_GET_SPECS = 'http://schedule.iitu.kz/rest/user/get_specialty.php?course=%d';

    const URL_GET_BLOCKS = 'http://schedule.iitu.kz/rest/user/get_group.php?course=%d&specialty_id=%d';

    const URL_GET_TIMETABLE = 'http://schedule.iitu.kz/rest/user/get_timetable_block.php?block_id=%d';

    const URL_GET_TEACHER_TIMETABLE = 'http://schedule.iitu.kz/rest/user/get_timetable_teacher.php?teacher_id=%d';

    public static function getSpecs($course)
    {
        $data = json_decode(
            file_get_contents(
                sprintf(
                    self::URL_GET_SPECS,
                    $course
                )
            )
        );

        if ($data->status != 0) {
            Log::info($data->error_message);
        }

        return $data->result;
    }

    public static function getBlocks($course, $spec_id)
    {
        $data = json_decode(
            file_get_contents(
                sprintf(
                    self::URL_GET_BLOCKS,
                    $course,
                    $spec_id
                )
            )
        );

        if ($data->status != 0) {
            Log::info($data->error_message);
        }

        return $data->result;
    }

    public static function getBlockData($block_id)
    {
        $data = json_decode(
            file_get_contents(
                sprintf(
                    self::URL_GET_TIMETABLE,
                    $block_id
                )
            ),
            true
        );

        if ($data['status'] != 0) {
            Log::info($data['error_message']);
        }

        return $data;
    }

    public static function getTeacherData($teacher_id)
    {
        $data = json_decode(
            file_get_contents(
                sprintf(
                    self::URL_GET_TEACHER_TIMETABLE,
                    $teacher_id
                )
            ),
            true
        );

        if ($data['status'] != 0) {
            Log::info($data['error_message']);
        }

        return $data;
    }
}
