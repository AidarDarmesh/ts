<?php

namespace App;

class Timetable
{
    const WEEK_DAYS = [
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
        '7' => 'Sunday',
    ];

    public static function prepareMessage($data, $row)
    {
        return sprintf(
            "🕐 %s\n📗 %s (%s)\n📍 %s\n👩‍🏫 %s\n\n",
            substr(
                $data['times'][$row[0]['time_id']]['start_time'], 0, 5
            ) . ' - ' .
            substr(
                $data['times'][$row[0]['time_id']]['end_time'], 0,5
            ),
            $data['subjects'][$row[0]['subject_id']]['subject_en'],
            substr(
                $data['subject_types'][$row[0]['subject_type_id']]['subject_type_en'], 0, 4
            ),
            $data['bundles'][$row[0]['bundle_id']][0]['name_en'],
            explode(
                ' ',
                $data['teachers'][(int) $row[0]['teacher_id']]['teacher_en']
            )[0]
        );
    }
}
