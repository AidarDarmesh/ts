<?php

namespace App;

use App\Button;

class Keyboard
{
    const MAX_PER_ROW = 3;

    const MAX_ROWS = 10;

    public static function create(array $buttons)
    {
        $keyboard   = [
            'one_time' => true,
            'buttons'  => [],
        ];
        $btnsNumber = count($buttons);
        $btnsPerRow = [];
        $counter    = 0;

        while ($counter < $btnsNumber) {
            $btnsPerRow[] = $buttons[$counter];

            if (($counter % self::MAX_PER_ROW == self::MAX_PER_ROW - 1) ||
                ($counter == $btnsNumber - 1)) {
                $keyboard['buttons'][] = $btnsPerRow;
                $btnsPerRow            = [];
            }

            $counter++;
        }

        return $keyboard;
    }

    public static function main()
    {
        $btnToday = Button::create(
            json_encode(
                [
                    'command'  => 'today',
                ]
            ),
            'today',
            'primary'
        );
        $btnTomorrow = Button::create(
            json_encode(
                [
                    'command'  => 'tomorrow',
                ]
            ),
            'tomorrow',
            'primary'
        );
        $btnWeek = Button::create(
            json_encode(
                [
                    'command'  => 'week',
                ]
            ),
            'week',
            'primary'
        );
        $btnRestart = Button::create(
            json_encode(
                [
                    'command'  => 'start',
                ]
            ),
            'restart',
            'primary'
        );
        $btnTeachers = Button::create(
            json_encode(
                [
                    'command'  => 'teachers',
                ]
            ),
            'teachers',
            'positive'
        );

        return [
            'one_time' => false,
            'buttons'  => [
                [
                    $btnToday,
                    $btnTomorrow,
                ],
                [
                    $btnWeek,
                    $btnRestart
                ],
                [
                    $btnTeachers
                ],
            ],
        ];
    }
}
