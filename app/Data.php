<?php

namespace App;

class Data
{
    const DEGREE_COURSES = [
        'bachelor' => [1, 2, 3, 4],
        'master'   => [1, 2],
        'doctor'   => [1, 2, 3],
    ];
}
