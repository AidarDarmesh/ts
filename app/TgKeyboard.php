<?php

namespace App;

use App\TgButton;

class TgKeyboard
{
    const MAX_PER_ROW = 3;

    const MAX_ROWS = 10;

    public static function create(array $buttons)
    {
        $keyboard   = [
            'inline_keyboard'  => [],
        ];
        $btnsNumber = count($buttons);
        $btnsPerRow = [];
        $counter    = 0;

        while ($counter < $btnsNumber) {
            $btnsPerRow[] = $buttons[$counter];

            if (($counter % self::MAX_PER_ROW == self::MAX_PER_ROW - 1) ||
                ($counter == $btnsNumber - 1)) {
                $keyboard['inline_keyboard'][] = $btnsPerRow;
                $btnsPerRow            = [];
            }

            $counter++;
        }

        return $keyboard;
    }

    public static function main()
    {

    }
}
