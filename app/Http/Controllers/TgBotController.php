<?php

namespace App\Http\Controllers;

use App\Jobs\HandleTgMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TgBotController extends Controller
{
    public function messageNew(Request $request)
    {
        Log::info($request->getContent());
        // Получаем и декодируем уведомление
        $data = json_decode($request->getContent());

        dispatch(new HandleTgMessage($data->message));
    }
}
