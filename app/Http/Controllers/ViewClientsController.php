<?php

namespace App\Http\Controllers;

use App\Client;
use App\Order;
use Illuminate\Http\Request;

class ViewClientsController extends Controller
{
    /**
     * Вью всех клиентов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	$clients = Client::all();

    	return view('clients.index', [
    								'clients' => $clients
    								]);
    }

    /**
     * Вью для добавления клиента
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addGet()
    {
    	return view('clients.add');
    }

    /**
     * Пост обработка добавления клиента
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function addPost(Request $request)
    {
    	$info = $request->all();

    	if (Client::find($info['id']) === null)
    	{
            $client           = new Client();
            $client->id       = $info['id'];
            $client->first    = $info['first'];
            $client->last     = $info['last'];
            $client->username = $info['username'];

			$client->save();

			return redirect('clients');
    	} else {
    		return "Клиент существует";
    	}
    }

    /**
     * Вью деталей клиента
     *
     * @param Client $client
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details(Client $client)
	{
		return view('clients.details', [
								'client' => $client,
								'orders' => $client->orders
								]);
	}

    /**
     * Вью редактирования клиента
     *
     * @param Client $client
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function editGet(Client $client)
	{
		return view('clients.edit', [
							'client' => $client
							]);
	}

    /**
     * Пост обработка редактирования клиента
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function editPost(Request $request)
	{
        $info             = $request->all();
        $client           = Client::find($info['id']);
        $client->first    = $info['first'];
        $client->last     = $info['last'];
        $client->username = $info['username'];

        $client->save();

        return redirect('clients/' . $client->id);
	}

    /**
     * Удаление клиента
     *
     * @param Client $client
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Client $client)
	{
		$client->delete();

		return redirect('clients');
	}
}
