<?php

namespace App\Http\Controllers;

use App\Jobs\HandleVkMessage;
use Illuminate\Http\Request;

class VkBotController extends Controller
{
	public function messageNew(Request $request)
	{
		// Получаем и декодируем уведомление
		$data = json_decode($request->getContent());

		switch ($data->type) {
			case "confirmation":
				// Отвечаем ВК API
				return config('vk.confkey');
			case "message_new":
			    dispatch(new HandleVkMessage($data->object));

				return "ok";
			default:
				return "ok";
		}
	}
}
