<?php

namespace App;

class Button
{
    public static function create($payload, $label, $color)
    {
        return [
            'color' => $color,
            'action' => [
                'type' => 'text',
                'payload' => $payload,
                'label' => $label,
            ],
        ];
    }
}
