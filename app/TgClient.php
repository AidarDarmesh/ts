<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class TgClient extends Model
{
    public $incrementing = false;

    const QUERY = "https://api.telegram.org/bot655228229:AAEGpwrFN_2LWpTrY-CL6d1MpJGVNbwM7jY/%s?%s";

    public function signUp($info)
    {
        $this->id       = $info->id;
        $this->first    = $info->first_name;
        $this->last     = $info->last_name;
        $this->username = $info->username;
        $this->state    = 'start';

        return $this->save();
    }

    public function send($message, $keyboard = null)
    {
        $requestParams            = [];
        $requestParams['chat_id'] = $this->id;
        $requestParams['text']    = $message;
        $requestParams['reply_markup'] = json_encode($keyboard);
        $httpParams               = http_build_query($requestParams);
        Log::info($requestParams);

        file_get_contents(
            sprintf(
                self::QUERY,
                'sendMessage',
                $httpParams
            )
        );
    }
}
