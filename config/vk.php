<?php
	
	return [

		// Access token для API ВК
		'token' => env('VK_TOKEN', '946f99f73a5a77583ec3296b9c516f35ae651c0899d8bd7363bddeadf33a581b76047bfacfbff78ce56a0'),

		// Ключ конфигурации
		'confkey' => env('VK_CONF', '505e1c85'),

		// Секретный ключ
		'secret' => env('VK_SECRET', 'aa98sd923j23j02fsdf'),

		// Версия ВК API
		'v' => env('VK_V', '5.70'),

		// Токен ссылок
		'url' => env('VK_URL', 'fX5bTv9JCrRukHUZ')

	];
